﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour {
   
    public EnemyBehaviour Spawner;
	// Use this for initialization
	void Start () {
        Spawner = GameObject.FindWithTag("Spawner").GetComponent<EnemyBehaviour>();
       
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter2D(Collider2D col)
    {
        if(col.gameObject.tag == "Enemy")
        {
            int index = int.Parse(col.gameObject.name);
            GameLogic.Utilities.util.MoveAway(Spawner.Pooler.Enemies[index], gameObject.transform.position, .35f);
            Spawner.Alive[index] = false;
            
        }
    }
}
