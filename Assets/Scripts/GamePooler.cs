﻿using UnityEngine;
using System.Collections;

public class GamePooler : MonoBehaviour {
    public int Max_Reticles;
    public int Max_Bullets;
    public int Max_Explosions;
    public int Max_Enemies;
    public int Max_Trails;

    public GameObject Bullet;
    public GameObject Reticle;
    public GameObject Explosion;
    public GameObject Enemy;
    public GameObject Trail;

    public GameObject[] Reticles;
    public GameObject[] Bullets;
    public GameObject[] Explosions;
    public GameObject[] Enemies;

    public GameObject[] Trails;
	// Use this for initialization
	void Start () {
        Reticles = new GameObject[Max_Reticles];
        for(int i = 0; i < Max_Reticles; i++)
        {
            Reticles[i] = (GameObject) Instantiate(Reticle, transform.position, transform.rotation);
            Reticles[i].SetActive(false);
        }

        Bullets = new GameObject[Max_Bullets];
        for(int i = 0; i < Max_Bullets; i++)
        {
            Bullets[i] = (GameObject)Instantiate(Bullet, transform.position, transform.rotation);
            Bullets[i].SetActive(false);
        }

        Explosions = new GameObject[Max_Explosions];
        for(int i = 0; i < Max_Explosions; i++)
        {
            Explosions[i] = (GameObject)Instantiate(Explosion, transform.position, transform.rotation);
            Explosions[i].SetActive(false);
        }

        Enemies = new GameObject[Max_Enemies];
        for (int i = 0; i < Max_Enemies; i++)
        {
            Enemies[i] = (GameObject)Instantiate(Enemy, transform.position, transform.rotation);
            Enemies[i].SetActive(false);
        }

        Trails = new GameObject[Max_Trails];

        for(int i = 0; i < Max_Trails; i++)
        {
            Trails[i] = (GameObject)Instantiate(Trail, transform.position, transform.rotation);
            Trails[i].SetActive(false);
        }
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
