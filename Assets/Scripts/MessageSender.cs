﻿using UnityEngine;
using System.Collections;

public class MessageSender : MonoBehaviour {
    public GameObject MessageReceiver;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    public void DeliverMessage(string msg)
    {
        MessageReceiver.gameObject.SendMessage(msg);
    }
}
