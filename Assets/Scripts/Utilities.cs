﻿using UnityEngine;
using System.Collections;

namespace GameLogic
{
    public class Utilities : MonoBehaviour
    {
        public static Utilities util;
        public AnimationClip shot;
        public AnimationClip explode;

        public bool[] liveTrails;
        private static float time;
        // Use this for initialization
        void Start()
        {
            util = gameObject.GetComponent<Utilities>();
            liveTrails = new bool[7];
        }

        // Update is called once per frame
        void Update()
        {

        }

        
        public  void _Deactivate(GameObject _obj, float _time)
        {

            StartCoroutine(Deactivate(_obj, _time));
        }
          IEnumerator Deactivate(GameObject obj, float time)
        {
            yield return new WaitForSeconds(time);
            obj.SetActive(false);
        }
        public float GetDistance(Vector3 target, Vector3 init)
        {
            float dist = Mathf.Sqrt((Mathf.Pow(target.x - init.x, 2)) + (Mathf.Pow(target.y - init.y , 2)));
            return dist;
        }

        public void _MoveTo(Vector3 target, GameObject init, int _index, GameObject explosion = null)
        {
            StartCoroutine(MoveTo(target, init, _index, explosion));
        }
        IEnumerator MoveTo(Vector3 target, GameObject init, int index, GameObject explosion = null)
        {
            if(index != -1)
            {
                liveTrails[index] = true;
            }
            while(GetDistance(target, init.gameObject.transform.position) > .01f)
            {
                init.transform.position = Vector3.MoveTowards(init.transform.position, target, 6 * Time.deltaTime);
                yield return new WaitForSeconds(.03f);
            }

            if(init.tag == "Bullet")
            {
                init.gameObject.GetComponent<TrailRenderer>().Clear();
                liveTrails[index] = false;
                explosion.transform.position = init.transform.position;
                explosion.SetActive(true);
                _Deactivate(explosion, explode.length);
                StartCoroutine(Deactivate(init, 0.0f));
               
                
            }
        }

        public void _SetTrailTime(TrailRenderer _trail , float _dissapate, int _index)
        {
            StartCoroutine(WaitForDissapate(_trail, _dissapate, _index));
        }
        IEnumerator SetTrailTime(TrailRenderer trail, float dissapate, int index)
        {
            while(trail.time > .05)
            {
                yield return new WaitForSeconds(.03f);
                trail.time -= (Time.deltaTime / dissapate) * 2.25f;
                if(liveTrails[index] == false)
                {
                    break;
                }
            }
        }

        IEnumerator WaitForDissapate(TrailRenderer _t, float _d, int _i)
        {
            yield return new WaitForSeconds(.3f);
            StartCoroutine(SetTrailTime(_t, _d, _i));
        }

        public void FadeEnemyTrail(SpriteRenderer enemyTrail)
        {
            StartCoroutine(_FadeEnemyTrail(enemyTrail));
        }

        IEnumerator _FadeEnemyTrail(SpriteRenderer _enemyTrail)
        {
            while(_enemyTrail.color.a > .01f)
            {
                yield return new WaitForSeconds(.03f);
                _enemyTrail.color = new Color(1, 1, 1, Mathf.Lerp(_enemyTrail.color.a, 0.0f, Time.deltaTime * 1.25f));
            }
            _Deactivate(_enemyTrail.gameObject, 0.0f);
        }

        public void MoveAway(GameObject init, Vector3 target , float limit = 0.0f)
        {
            StartCoroutine(_MoveAway(init, target, limit));
        }

        IEnumerator _MoveAway(GameObject _init, Vector3 _target, float _limit = 0.0f)
        {
            
            float max_loops = _limit / .03f;
            float current_loops = 0.0f;
            while(current_loops < max_loops)
            {
                _init.transform.position = Vector3.MoveTowards(_init.transform.position, _target, Time.deltaTime * -7.75f);
                current_loops++;
                yield return new WaitForSeconds(.03f);

            }
        }
    }
}
