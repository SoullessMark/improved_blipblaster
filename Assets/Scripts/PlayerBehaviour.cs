﻿using UnityEngine;
using System.Collections;

public class PlayerBehaviour : MonoBehaviour {
    public GamePooler Pooler;
    public Animator[] Arrows;
    public Animator[] Turrets;
    private int Active_Turret;
    private bool[] Activated;
    private int Last_Active_Turret;
    private bool Target_Selected = false;
    private Vector3 target;
	// Use this for initialization
	void Start () {
        Active_Turret = -1;
        Activated = new bool[3];
        StartCoroutine(CheckInput());
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    IEnumerator CheckInput()
    {
       bool running = true;

       while (running) {
            if (Input.GetMouseButtonDown(0))
            {
                if (!Target_Selected)
                {
                    Vector3 temp = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    if (temp.y > -1.25)
                    {
                        Target_Selected = true;
                        StartCoroutine(SelectTarget());
                    }
                }
            }
            yield return new WaitForSeconds(.005f);

        }
    }

    IEnumerator SelectTarget()
    {
        if (Active_Turret > -1)
        {
            for (int i = 0; i < Pooler.Max_Reticles; i++)
            {
                if (Pooler.Reticles[i].activeInHierarchy == false)
                {
                    if (Activated[Active_Turret] == false)
                    {
                        Vector3 temp = Pooler.Reticles[i].gameObject.transform.position = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                        target = Pooler.Reticles[i].gameObject.transform.position = new Vector3(temp.x, temp.y, 0);
                        Pooler.Reticles[i].gameObject.SetActive(true);
                        GameLogic.Utilities.util._Deactivate(Pooler.Reticles[i].gameObject, 2.5f);
                        SetTurretRotation(target, Turrets[Active_Turret].gameObject.transform.position);
                        SetTurretAnimationState(Turrets[Active_Turret]);
                        Activated[Active_Turret] = true;
                        break;
                    }
                }
            }
        }
        yield return new WaitForSeconds(.45f);
        Target_Selected = false;
    }

    public void Activate_Selection_Arrow(int arrow)
    {
        for(int i = 0; i < 3; i++)
        {
            Arrows[i].SetInteger("State", 0);
        }
        Arrows[arrow].SetInteger("State", 1);
        Active_Turret = arrow;
        
    }

    public void Shoot()
    {
        for(int i = 0; i < Pooler.Max_Bullets; i++)
        {
            if(Pooler.Bullets[i].activeInHierarchy == false)
            {
                GameObject Available_Explosion = null;
                for(int k = 0; k < Pooler.Max_Explosions; k++)
                {
                    if(Pooler.Explosions[k].activeInHierarchy == false)
                    {
                        Available_Explosion = Pooler.Explosions[k];
                        break;
                    }
                }
                Pooler.Bullets[i].gameObject.transform.position = Turrets[Active_Turret].gameObject.transform.GetChild(0).transform.position;
                Pooler.Bullets[i].gameObject.transform.rotation = Turrets[Active_Turret].gameObject.transform.rotation;
                Pooler.Bullets[i].SetActive(true);
                TrailRenderer trail = Pooler.Bullets[i].gameObject.GetComponent<TrailRenderer>();
                trail.time = (GameLogic.Utilities.util.GetDistance(target, trail.gameObject.transform.position) / 6.75f); 
                GameLogic.Utilities.util._MoveTo(target, Pooler.Bullets[i].gameObject, i, Available_Explosion);
                GameLogic.Utilities.util._SetTrailTime(trail , trail.time / .75f, i);
                break;
            }
        }
    }
    void SetTurretRotation(Vector3 target , Vector3 init)
    {
        Vector3 diff = target - init;
        diff.Normalize();
        float rot_z = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
        Turrets[Active_Turret].gameObject.transform.rotation = Quaternion.Euler(0f, 0f, rot_z - 90);
    }
    void SetTurretAnimationState(Animator turret)
    {
        turret.SetInteger("State", 1);
        Last_Active_Turret = Active_Turret;
        StartCoroutine(ResetAnim(turret));
    }

    IEnumerator ResetAnim(Animator _turret)
    {
        yield return new WaitForSeconds(GameLogic.Utilities.util.shot.length);
        _turret.SetInteger("State", 0);
        Activated[Last_Active_Turret] = false;
    }
}
