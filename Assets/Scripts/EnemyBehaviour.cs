﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBehaviour : MonoBehaviour {
    public int Active_Enemies;
    public int Max_Active_Enemies;
    public GamePooler Pooler;
    public PlayerBehaviour player;
    public bool running = true;
    public bool[] Alive;
    public float[] Speed;
    public Sprite[] Sprites;


	// Use this for initialization
	void Start () {

        StartCoroutine(AfterInitialization());
	}
	
	// Update is called once per frame
	void Update () {
		
	}
   
    IEnumerator AfterInitialization()
    {
        yield return new WaitForSeconds(1.5f);
        Alive = new bool[Pooler.Max_Enemies];
        Speed = new float[Pooler.Max_Enemies];

        StartCoroutine(RunTime());
    }

    IEnumerator RunTime()
    {
        while (running)
        {
            yield return new WaitForSeconds(5.0f);
            Active_Enemies = 0;
            for(int i = 0; i < Pooler.Max_Enemies; i++)
            {
                if(Pooler.Enemies[i].activeInHierarchy == true)
                {
                    Active_Enemies += 1;
                }
                if(Pooler.Enemies[i].activeInHierarchy == false && Active_Enemies < Max_Active_Enemies)
                {
                    Pooler.Enemies[i].gameObject.transform.position = new Vector3(Random.Range(-5.5f, 5.5f), transform.position.y - .5f, transform.position.z);
                    Pooler.Enemies[i].SetActive(true);
                    Alive[i] = true;
                    Speed[i] = 1.25f;
                    DetermineEnemyType(Random.Range(1, 4), Pooler.Enemies[i].gameObject, i);
                    break;
                }
            }
        }
    }

    public void DetermineEnemyType(int rand , GameObject enemy, int index)
    {
        enemy.name = index.ToString();
        DetermineRotation(rand, enemy);
        if(rand == 1)
        {
            enemy.gameObject.GetComponent<SpriteRenderer>().sprite = Sprites[0];
        }else if(rand == 2)
        {
            enemy.gameObject.GetComponent<SpriteRenderer>().sprite = Sprites[1];
        }
        else if(rand  == 3)
        {
            enemy.gameObject.GetComponent<SpriteRenderer>().sprite = Sprites[2];
        }

        StartCoroutine(Basic(enemy, index));
    }

    public void DetermineRotation(int _rand, GameObject _enemy)
    {
        if (_rand == 3)
        {
            Vector3 target = player.Turrets[Random.Range(0, 3)].gameObject.transform.position;
            Vector3 diff = target - _enemy.gameObject.transform.position;
            diff.Normalize();
            float rot_z = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
            _enemy.gameObject.transform.rotation = Quaternion.Euler(0f, 0f, rot_z + 90);
        }
        else
        {
            Vector3 euler = _enemy.transform.eulerAngles;
            euler.z = Random.Range(-15.0f, 15.0f);
            _enemy.gameObject.transform.eulerAngles = euler;
        }
        
    }

    IEnumerator Basic(GameObject _enemy , int _index)
    {
        //Add StartupAnimMethod
        int FloatTracker = 0;
        while(running)
        {
            FloatTracker++;
            if(FloatTracker >= 50)
            {
               for(int i = 0; i < Pooler.Max_Trails; i++)
                {
                    if(Pooler.Trails[i].activeInHierarchy == false)
                    {
                        Pooler.Trails[i].SetActive(true);
                        Pooler.Trails[i].transform.position = _enemy.transform.position;
                        Pooler.Trails[i].transform.rotation = _enemy.transform.rotation;
                        SpriteRenderer grab = Pooler.Trails[i].GetComponent<SpriteRenderer>();
                        grab.color = new Color(1f, 1f, 1f, 1f);
                        GameLogic.Utilities.util.FadeEnemyTrail(grab);
                        
                        break;
                    }
                }
                FloatTracker = 0;
            }
            yield return new WaitForSeconds(.03f);

            if(_enemy.transform.position.y > -3.5f)
            {
                _enemy.transform.Translate(Vector3.down * ((Time.deltaTime * Speed[_index] / 2)));
            }
            

            if(_enemy.transform.position.y < -3.5f)
            {
                //Add a death method with anim
                Alive[_index] = false;
                
               
            }

            if(Alive[_index] == false)
            {
                StartCoroutine(Enemy_Die(_enemy, "Goal", _index));
                break;
                
            }
        }
    }

    IEnumerator Circle(GameObject _enemy, int _index)
    {
        while (running)
        {
            yield return new WaitForSeconds(1.5f);
            if (Alive[_index] == false)
            {
                break;
            }
        }
       
    }

    IEnumerator Square(GameObject _enemy, int _index)
    {
        while (running)
        {
            yield return new WaitForSeconds(.5f);
            if (Alive[_index] == false)
            {
                break;
            }
        }

    }

    IEnumerator Triangle(GameObject _enemy, int _index)
    {
        while (running)
        {
            yield return new WaitForSeconds(.5f);
            if (Alive[_index] == false)
            {
                break;
            }
        }

    }

    IEnumerator Enemy_Die(GameObject _enemy, string _reason, int _index, GameObject _cause = null)
    {
        yield return new WaitForSeconds(.75f);
        GameLogic.Utilities.util._Deactivate(_enemy, 0.0f);
        Active_Enemies -= 1;
    }
}
